package main;
/*
  B-------C
 /       /
A-------D
*/

public class Quadrangle extends Shape {
    private Point pointA;
    private Point pointB;
    private Point pointC;
    private Point pointD;

    public Quadrangle(Point pointA, Point pointB, Point pointC, Point pointD){
        buildIt(pointA, pointB, pointC, pointD);
    }

    @Override
    public double getPerimeter() {
        return pointToPointLength(pointA, pointB) + pointToPointLength(pointB, pointC) + pointToPointLength(pointC, pointD) + pointToPointLength(pointD, pointA);
    }

    @Override
    public double getArea() {
        return 0.5 * Math.abs(pointA.getX()*pointB.getY() + pointB.getX()*pointC.getY() + pointC.getX()*pointD.getY() + pointD.getX()*pointA.getY() -
                pointB.getX()*pointA.getY() - pointB.getX()*pointA.getY() - pointD.getX()*pointC.getY() - pointA.getX()*pointD.getY());
    }

    private void buildIt(Point a, Point b, Point c, Point d){
        int abc = checkRotation(a, b, c);
        int abd = checkRotation(a, b, d);
        int acd = checkRotation(a, c, d);

        if (((abc != 0) & (abd != 0)) & (abc*abd>0)){
            if (abc < 0){
                setLocation(a,b,c,d);
            } else {
                setLocation(d,c,b,a);
            }
        }
        if (((abc != 0) & (acd != 0)) & (abc*acd>0)){
            if (abc < 0){
                setLocation(a,b,c,d);
            } else {
                setLocation(d,c,b,a);
            }
        }
        if (((acd != 0) & (abd != 0)) & (acd*abd>0)){
            if (acd < 0){
                setLocation(a,c,d,b);
            } else {
                setLocation(b,d,c,a);
            }
        }
    }

    private int checkRotation(Point a, Point b, Point c){
        double darea = a.getX()*b.getY() + b.getX()*c.getY() + c.getX()*a.getY() - b.getX()*a.getY() - c.getX()*b.getY() - a.getX()*c.getY();
        if (darea < 0){
            return -1;
        } else {
            if (darea > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    private void setLocation(Point a, Point b, Point c, Point d){
        this.pointA = a;
        this.pointB = b;
        this.pointC = c;
        this.pointD = d;
    }
}
