package main;

public class Board {
    private Shape[] part = new Shape[4];

    public void add(Shape shape, int ind){
        if ((ind > 0) & (ind < 5)){
            if (part[ind-1] == null){
                part[ind-1] = shape;
                System.out.println(shape.getClass().getName().substring(5) + " успешно добавлена на " + ind + "-ю часть доски");
            } else {
                System.out.println(ind + "-я часть доски занята");
            }
        } else {
            System.out.println("Введен некоректный индекс");
        }
    }

    public void delete(int ind){
        if (part[ind-1] != null){
            String name = part[ind-1].getClass().getName().substring(5);
            part[ind-1] = null;
            System.out.println(name + " успешно удалена с " + ind + "-й части доски");
        }
    }

    public String getInfo(){
        String shapesInfo = "";
        double shapesArea = 0;

        for (int i = 0; i < part.length; i++) {
            if (part[i] != null){
                shapesInfo += part[i].getClass().getName().substring(5) + "с площадью " + String.format("%.2f", part[i].getArea()) + " и периметром " + String.format("%.2f", part[i].getPerimeter()) + ";\n";
                shapesArea += part[i].getArea();
            }
        }
        return "\nНа доске лежат : \n" + shapesInfo + "Общая площадь всех фигур " + String.format("%.2f", shapesArea);
    }
}
