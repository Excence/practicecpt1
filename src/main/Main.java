package main;

public class Main {
    public static void main(String[] args) {


        Board board = new Board();
        board.add(new Circle(new Point(0,0), new Point(3,2)), 0);
        board.add(new Circle(new Point(0,0), new Point(3,2)), 1);
        board.add(new Triangle(new Point(0,0), new Point(3,2), new Point(0, 2)), 2);
        board.add(new Triangle(new Point(1,3), new Point(3,2), new Point(5, 7)), 3);
        board.add(new Quadrangle(new Point(2,1), new Point(3,5), new Point(6,4),  new Point(5, 2)), 4);
        board.delete(2);
        System.out.println(board.getInfo());
    }
}
