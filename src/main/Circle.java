package main;

public class Circle extends Shape {
    Point center;
    Point onRing;

    public Circle(Point center, Point endOfRadius){
        this.center = center;
        this.onRing = endOfRadius;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * 2 *pointToPointLength(center, onRing);
    }

    @Override
    public double getArea() {
        return 3.14 * Math.pow(pointToPointLength(center, onRing), 2);
    }

    @Override
    protected double pointToPointLength(Point a, Point b) {
        return super.pointToPointLength(a, b);
    }
}
