package main;

public class Triangle extends Shape{
    private Point pointA;
    private Point pointB;
    private Point pointC;

    public Triangle(Point pointA, Point pointB, Point pointC){
        if (isExist(pointA, pointB, pointC)){
            this.pointA = pointA;
            this.pointB = pointB;
            this.pointC = pointC;
        }
    }

    @Override
    public double getPerimeter() {
        return pointToPointLength(pointA, pointB) + pointToPointLength(pointB, pointC) + pointToPointLength(pointC, pointA);
    }

    @Override
    public double getArea() {
        return 0.5 * Math.abs(pointA.getX()*pointB.getY() + pointB.getX()*pointC.getY() + pointC.getX()*pointA.getY() -
                pointB.getX()*pointA.getY() - pointC.getX()*pointB.getY() - pointA.getX()*pointC.getY());
    }

    private boolean isExist(Point a, Point b, Point c){
        double darea = a.getX()*b.getY() + b.getX()*c.getY() + c.getX()*a.getY() - b.getX()*a.getY() - c.getX()*b.getY() - a.getX()*c.getY();
        return darea != 0;
    }
}
