package main;

abstract class Shape {
    public abstract double getPerimeter();

    public abstract double getArea();

    protected double pointToPointLength(Point a, Point b){
        return Math.sqrt(Math.pow((a.getX()-b.getX()),2) + Math.pow((a.getY()-b.getY()),2));
    }
}
